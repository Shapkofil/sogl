#include<stdint.h>
#include<stdio.h>
#include<sys/time.h>
#include<stdlib.h>
#include<X11/X.h>
#include<X11/Xlib.h>
#include<GL/gl.h>
#include<GL/glx.h>
#include<GLES3/gl3.h>
#include<EGL/egl.h>


const char *vertexshadersource = "#version 330 core\n"
    "layout (location = 0) in vec3 aPos;\n"
    "void main()\n"
    "{\n"
    "   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
    "}\0";

const char *fragmentshadersource = "#version 330 core\n"
    "out vec4 FragColor;\n"
    "void main()\n"
    "{\n"
    "   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
    "}\n\0";

float timefs(struct timeval startt) {
    struct timeval tv;
    gettimeofday(&tv,NULL);

    return (float)(tv.tv_sec - startt.tv_sec ) + 
      (float)(tv.tv_usec - startt.tv_usec)/1000000.0f;
}

char *slurp_file_into_malloced_cstr(const char *file_path)
{
    FILE *f = NULL;
    char *buffer = NULL;

    f = fopen(file_path, "r");
    if (f == NULL) goto fail;
    if (fseek(f, 0, SEEK_END) < 0) goto fail;

    long size = ftell(f);
    if (size < 0) goto fail;

    buffer = malloc(size + 1);
    if (buffer == NULL) goto fail;
    if (fseek(f, 0, SEEK_SET) < 0) goto fail;

    fread(buffer, 1, size, f);
    if (ferror(f)) goto fail;

    buffer[size] = '\0';

    if (f)
        fclose(f);
    return buffer;
fail:
    if (f)
        fclose(f);
    if (buffer)
        free(buffer);
    return NULL;
}


void compile_shader_file(unsigned int shadertype, const char * filepath, unsigned int *shader){
  const GLchar *source = slurp_file_into_malloced_cstr(filepath);
  *shader = glCreateShader(shadertype);
  glShaderSource(*shader, 1, &source, NULL);
  glCompileShader(*shader);

  int success;
  char infoLog[512];
  glGetShaderiv(*shader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
      glGetShaderInfoLog(*shader, 512, NULL, infoLog);
      printf("ERROR::%s::SHADER::COMPILATION_FAILED\n %s\n",
	     shadertype == GL_VERTEX_SHADER ? "VERTEX" : "FRAGMENT",
	     infoLog);
  }
}

void compile_shader_source(unsigned int shadertype, const GLchar * source, unsigned int *shader){
  *shader = glCreateShader(shadertype);
  glShaderSource(*shader, 1, &source, NULL);
  glCompileShader(*shader);

  int success;
  char infoLog[512];
  glGetShaderiv(*shader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
      glGetShaderInfoLog(*shader, 512, NULL, infoLog);
      printf("ERROR::%s::SHADER::COMPILATION_FAILED\n %s\n",
	     shadertype == GL_VERTEX_SHADER ? "VERTEX" : "FRAGMENT",
	     infoLog);
  }
}

unsigned int VBO, VAO;
unsigned int EBO;
unsigned int vertexShader;
unsigned int fragmentShader;
unsigned int shaderProgram;

void setupGL(){
  printf("%s\n", glGetString(GL_VERSION));

  float vertices[] = {
    .5f , -.5f, .5f,
    -.5f, -.5f, .5f,
    -.5f,  .5f, .5f,
    .5f ,  .5f, .5f
  };  

  unsigned int indices[] = {
    0, 1, 2,
    2, 3, 0
  };

  compile_shader_source(GL_VERTEX_SHADER, vertexshadersource, &vertexShader);
  compile_shader_file(GL_FRAGMENT_SHADER, "test.frag", &fragmentShader);
  
  shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glLinkProgram(shaderProgram);

  int success;
  char infoLog[512];
  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
  if (!success) {
      glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
      printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n %s\n", infoLog);
  }

  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);  

  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);

  glBindVertexArray(VAO);
  
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
  glEnableVertexAttribArray(0);
  
  glBindBuffer(GL_ARRAY_BUFFER, 0); 
  
  glBindVertexArray(0); 

}

void destroyGL(){
  glDeleteVertexArrays(1, &VAO);
  glDeleteBuffers(1, &VBO);
  glDeleteBuffers(1, &EBO);
  glDeleteProgram(shaderProgram);
}

void setupUniforms(unsigned int program, struct timeval startt, XWindowAttributes gwa)
{
  int u_time_location = glGetUniformLocation(shaderProgram, "u_time");
  glUniform1f(u_time_location, timefs(startt));

  int u_resolution_location = glGetUniformLocation(shaderProgram, "u_resolution");
  glUniform2f(u_resolution_location, gwa.width, gwa.height);
}

void DrawAQuad() {
  glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  glUseProgram(shaderProgram);

  glBindVertexArray(VAO);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
} 


int main(){
  Display                 *dpy;
  Window                  root;
  GLint                   att[] = { GLX_RGBA,
				    GLX_DEPTH_SIZE, 24,
				    GLX_DOUBLEBUFFER, None };
  XVisualInfo             *vi;
  Colormap                cmap;
  XSetWindowAttributes    swa;
  Window                  w;
  GLXContext              glc;
  XWindowAttributes       gwa;
  XEvent                  xev;
  struct timeval          startt;

  gettimeofday(&startt,NULL);


  if (!(dpy = XOpenDisplay(NULL)))
		printf("slock: cannot open display\n");

  // Create Visual info for GL in dpy
  vi = glXChooseVisual(dpy, 0, att);
  root = XDefaultRootWindow(dpy);

  if(vi == NULL) {
    printf("\n\tno appropriate visual found\n\n");
    exit(0);
  } 
  else {
     /* %p creates hexadecimal output like in glxinfo */
    printf("\n\tvisual %p selected\n", (void *)vi->visualid);
  }

  // Creating a color map
  cmap = XCreateColormap(dpy, root, vi->visual, AllocNone);
  
  // X window attribute structure
  swa.colormap = cmap;
  swa.event_mask = ExposureMask | KeyPressMask;

  w = XCreateWindow(
		    dpy,
		    root,
		    0, 0,
		    600, 600,
		    0,
		    vi->depth,
		    InputOutput,
		    vi->visual,
		    CWColormap | CWEventMask, // Specifies the values of swa
		    &swa);


  // Mapping means displaying window
  XMapWindow(dpy, w);
  XStoreName(dpy, w, "sogl");

  // Creating GL Context
  glc = glXCreateContext(dpy, vi, NULL, GL_TRUE);
  glXMakeCurrent(dpy, w, glc);


  // X11 is asynchonis so we need to sync to display windows
  XSync(dpy,False);

  // Creating an atom to handle WM closing the app window
  Atom wm_delete_window = XInternAtom(dpy, "WM_DELETE_WINDOW", False);
  XSetWMProtocols(dpy, w, &wm_delete_window, 1);

  setupGL();

  //Handing events
  int quit=0;
  while(!quit){
    XNextEvent(dpy, &xev);
    XGetWindowAttributes(dpy, w, &gwa);
    glViewport(0, 0, gwa.width, gwa.height);
    setupUniforms(shaderProgram, startt, gwa);
    DrawAQuad();
    glXSwapBuffers(dpy, w);
    switch(xev.type){
      case Expose:{}
      case KeyPress:{
	// printf("Some key has been pressed\n");
      } break ;
      case ClientMessage:{
	if((Atom) xev.xclient.data.l[0] == wm_delete_window) {
	  printf("Window Manager killed window");
	  quit = 1;
	}
      };
    }
  }

  XCloseDisplay(dpy);

  destroyGL();

  return 0;
}

#version 330 core
out vec4 FragColor;
in highp vec4 gl_FragCoord;

uniform float u_time;
uniform vec2 u_resolution;

void main()
{

  vec2 p = gl_FragCoord.xy / u_resolution.xy;
  

  vec3 col = vec3(p, 0.0);
  //col = mix(col, 1.0 - col, abs(sin(u_time)));

  FragColor = vec4(col, 1.0); 
} 

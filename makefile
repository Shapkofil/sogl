CC=cc
LIBS=-lX11 -lGL -lGLU -lGLESv2

sogl : sogl.o
	${CC} -o sogl sogl.o ${LIBS}

sogl.o : sogl.c
	${CC} -c sogl.c ${LIBS}

clean :
	rm sogl
	rm sogl.o
